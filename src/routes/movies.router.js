const router = require("@koa/router");
const koaBody = require("koa-body");
const controller = require("./../controllers/movies.controller");
const {
  listMovieHeaderSchema,
  showMovieSchema,
  showMovieHeaderSchema,
  createMovieSchema,
} = require("./../schemas/movie.schemas");
const { searchReplaceSchema } = require("../schemas/search-replace.schema");
const {
  buildRequestValidator,
} = require("./../middlewares/buildRequestValidator");

const moviesRouter = router();

moviesRouter
  .get(
    "/movies",
    buildRequestValidator(listMovieHeaderSchema, "headers"),
    controller.list
  )
  .get(
    "/movies/:id",
    buildRequestValidator(showMovieSchema, "params"),
    buildRequestValidator(showMovieHeaderSchema, "headers"),
    controller.show
  )
  .post(
    "/movies",
    koaBody(),
    buildRequestValidator(createMovieSchema, "body"),
    controller.create
  )
  .post(
	"/movies/search-replace",
	koaBody(),
	buildRequestValidator(searchReplaceSchema, "body"),
	controller.searchReplace
  );

module.exports = { moviesRouter };
