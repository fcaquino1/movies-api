const Joi = require("joi");

const id = Joi.string().min(24).max(24);

const source = Joi.string();
const value = Joi.string();

const ratingMovieSchema = Joi.object({
	source: source.required(),
	value: value.required()
});

const title = Joi.string();
const year = Joi.string().min(4).max(4);
const released = Joi.string();
const genre = Joi.string();
const director = Joi.string();
const actors = Joi.string();
const plot = Joi.string();
const ratings = Joi.array().items(ratingMovieSchema);

const createMovieSchema = Joi.object({
	title: title.required(),
	year: year.required(),
	released: released.required(),
	genre: genre.required(),
	director: director.required(),
	actors: actors.required(),
	plot: plot.required(),
	ratings: ratings.required(),
});

const showMovieSchema = Joi.object({
  id: id.required(),
});

const showMovieHeaderSchema = Joi.object({
  year: year,
}).options({ allowUnknown: true });

const page = Joi.number().integer();

const listMovieHeaderSchema = Joi.object({
  page: page,
}).options({ allowUnknown: true });

module.exports = {
  showMovieSchema,
  showMovieHeaderSchema,
  listMovieHeaderSchema,
  createMovieSchema
};
