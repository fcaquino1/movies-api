const Joi = require('joi');

const movie = Joi.string().min(1);
const find = Joi.string().min(1);
const replace = Joi.string().min(1);

const searchReplaceSchema = Joi.object({
	movie: movie.required(),
	find: find.required(),
	replace: replace.required(),
});

module.exports = { searchReplaceSchema };