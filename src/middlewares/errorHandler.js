async function errorHandler(ctx, next) {
	try {
		await next();
	} catch (error) {
		if (error.isBoom) {
			ctx.status = error.output.statusCode;
			ctx.body = error.output.payload;
		} else {
			ctx.status = 500;
			ctx.body = {
				message: 'Internal Server Error'
			};
		}

		const logger = ctx.logger;
		let level = 'warn';
		if (ctx.status >= 500) {
			level = 'error';
		}

		logger.log(level, {
			message: error.message,
			stack: error.stack,
		});
	}
};

module.exports = { errorHandler };

