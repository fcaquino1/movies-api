const boom = require('@hapi/boom');

function buildRequestValidator(requestSchema, requestProperty) {
	return async (ctx, next) => {
		const data = ctx.request[requestProperty];
		const { error } = requestSchema.validate(data);
		if (error) {
			throw boom.badRequest(error.message);
		} else {
			await next();
		}
	};
}

module.exports = { buildRequestValidator };