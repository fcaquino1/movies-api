const { MovieModel } = require("./../db/models/movie");
const mongoose = require("mongoose");
const boom = require("@hapi/boom");

async function list(page, pageSize = 5) {
  return await MovieModel.find()
    .limit(pageSize)
    .skip(pageSize * page)
    .exec();
}

async function findOne(id, filters) {
  if (!mongoose.Types.ObjectId.isValid(id)) {
    throw boom.badRequest("Invalid Movie ID");
  }

  const query = MovieModel.findById(id);
  if (filters && filters.year) {
    query.and({ year: filters.year });
  }

  const movie = await query.exec();

  if (!movie) {
    throw boom.notFound("Movie not found");
  }

  return movie;
}

async function findFirst(movie) {
	const movieRecord = await MovieModel.find(movie).exec();
	if (!Array.isArray(movieRecord) || movieRecord.length <= 0) {
		return null;
	}
	return movieRecord.shift();
}

async function exists(movie) {
	let movieRecord = await findFirst(movie);
	if (!movieRecord) {
		movieRecord = await findFirst({
			title: movie.title,
			year: movie.year,
			released: movie.released,
			director: movie.director,
		});
	}
	return movieRecord ? true : false;
}

async function create(newMovie) {
  const movie = new MovieModel(newMovie);
  try {
	await movie.save();
  } catch (error) {
	throw boom.internal("Movie save failed");
  }
}

module.exports = {
  list,
  findFirst,
  findOne,
  create,
  exists,
};
