const Koa = require("koa");
const accessLogger = require('koa-logger');
const { getLogger } = require('./helpers/getLogger');
const { connectDB } = require('./db/connectDB');
const { errorHandler } = require("./middlewares/errorHandler");
const { moviesRouter } = require('./routes/movies.router');

const logger = getLogger();
connectDB(logger);

const app = new Koa();
app.context.logger = logger;

app.use(accessLogger());
app.use(errorHandler);
app.use(moviesRouter.routes());

app.listen(3000);