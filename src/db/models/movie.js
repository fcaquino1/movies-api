const mongoose = require('mongoose');

const movieSchema = new mongoose.Schema({
	title: mongoose.SchemaTypes.String,
	year: mongoose.SchemaTypes.String,
	released: mongoose.SchemaTypes.String,
	genre: mongoose.SchemaTypes.String,
	director: mongoose.SchemaTypes.String,
	actors: mongoose.SchemaTypes.String,
	plot: mongoose.SchemaTypes.String,
	ratings: mongoose.SchemaTypes.Array
});

module.exports = { MovieModel: mongoose.model('movies', movieSchema) };