const mongoose = require("mongoose");
const { database } = require("./../config/database")

function connectDB(logger) {
  mongoose
    .connect(database.connectionStrig, { dbName: database.name })
    .catch(function (error) {
      logger.log("error", {
        message: error.message,
        stack: error.stack,
      });
    });

  mongoose.connection.on("error", function (error) {
    logger.log("error", {
      message: error.message,
      stack: error.stack,
    });
  });
}

module.exports = { connectDB };
