const boom = require("@hapi/boom");
const moviesService = require("./../services/movies.service");

async function list(ctx) {
  const { page } = ctx.request.headers;

  let normalizedPage = 0;
  if (page) {
    normalizedPage = Math.max(normalizedPage, page);
  }

  ctx.body = await moviesService.list(normalizedPage);
}

async function show(ctx) {
  const { year } = ctx.request.headers;
  const id = ctx.params.id;
  ctx.body = await moviesService.findOne(id, { year });
}

async function create(ctx) {
  const body = ctx.request.body;
  const movie = await moviesService.exists(body);
  if (movie) {
    throw boom.badRequest("The movie is already exists");
  }
  await moviesService.create(body);
  ctx.status = 201;
  ctx.body = {
    message: "created",
  };
}

async function searchReplace(ctx) {
	const { movie, find, replace } = ctx.request.body;
	const movieRecord = await moviesService.findFirst({ title: movie });
	if (!movieRecord) {
		throw boom.notFound("Movie not found");
	}
	let plot = movieRecord.plot;
	if (plot) {
		const replacer = new RegExp(find, 'g');
		plot = plot.replace(replacer, replace);
	}
	ctx.status = 201;
	ctx.body = {
		plot: plot,
	};
}

module.exports = {
  list,
  show,
  create,
  searchReplace
};
