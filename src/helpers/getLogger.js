const winston = require("winston");
const format = winston.format;

function getLogger() {
  return winston.createLogger({
    format: format.combine(format.timestamp(), format.prettyPrint()),
    transports: [
      new winston.transports.File({ filename: "logs/app.log" }),
    ],
  });
}

module.exports = { getLogger };
