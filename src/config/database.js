const dotenv = require('dotenv');
dotenv.config();
const user = process.env.DB_USER || '';
const password = process.env.DB_PASSWORD || '';
const host = process.env.DB_HOST || '';
const port = parseInt(process.env.DB_PORT || '27017');
const name = process.env.DB_NAME || '';

const URI = `mongodb://${user}:${password}@${host}`;

module.exports = {
	database: {
		 name: name,
		 connectionStrig: URI,
	},
};