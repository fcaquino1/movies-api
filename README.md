## Development
1. Requirements docker and docker-compose
   - `Docker version 20.10.8, build 3967b7d`
---

### Database
2. Create a file `.env` file in root directory with following values for example
```env
MONGO_INITDB_ROOT_USERNAME=root
MONGO_INITDB_ROOT_PASSWORD=root
```
---

### API
3. Create a file `.env` file in root directory with following values for example
```env
DB_USER=root
DB_PASSWORD=root
DB_HOST=mongo_database
DB_PORT=27017
DB_NAME=clm_db
```
* The enviroments variables `DB_HOST` and `DB_PORT` must have `mongo_database` and `27017` as values to work correctly, everthing else
can be changed.
* The enviroments variables `MONGO_INITDB_ROOT_USERNAME` and `MONGO_INITDB_ROOT_PASSWORD` must be the same as `DB_USER` and `DB_PASSWORD`.
---

### Start project
3. To start the project run in the root the following command
```bash
docker-compose up
```